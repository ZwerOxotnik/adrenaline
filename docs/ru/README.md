# Адреналин

Хотите прочитать на другом языке? | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Содержание

* [Введение](#overview)
* [Версии](#versions)
* [Сообщить об ошибки](#issue)
* [Запросить функцию](#feature)
* [Будущие планы](#future-plans)
* [Установка](#installing)
* [Лицензия](#license)

## <a name="overview"></a> Введение

Адреналин обостряет ваши чувства при смертельной опасности. Ускоряет ваш бег и скорость добычи в разы.

## <a name="issue"></a> Нашли ошибку?

Пожалуйста, сообщайте о любых проблемах или ошибках в документации, вы можете помочь нам
[submitting an issue](https://gitlab.com/ZwerOxotnik/adrenaline/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/adrenaline/discussion).

## <a name="feature"></a> Хотите новую функцию?

Вы можете *запросить* новую функцию [submitting an issue](https://gitlab.com/ZwerOxotnik/adrenaline/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/adrenaline/discussion).

## <a name="future-plans"></a> Будущие планы

* Добавить удалённый интерфейс
* Улучшить совместимость со сценариями

## Установка

Если вы скачали zip архив:

* просто поместите его в директорию модов.

Для большей информации, смотрите [вики Factorio "загрузка и установка модов"](https://wiki.factorio.com/Modding/ru#.D0.97.D0.B0.D0.B3.D1.80.D1.83.D0.B7.D0.BA.D0.B0_.D0.B8_.D1.83.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B0_.D0.BC.D0.BE.D0.B4.D0.BE.D0.B2).

если вы скачали исходный архив (GitLab):

* скопируйте данный мод в директорию модов Factorio
* переименуйте данный мод в adrenaline_*версия*, где *версия* это версия мода, которую вы скачали (например, 1.3.0)

## Лицензия

Основная лицензия проекта - MIT
```
Copyright (c) 2019-2020 ZwerOxotnik <zweroxotnik@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

[homepage]: http://mods.factorio.com/mod/adrenaline
[Factorio]: https://factorio.com/
