# Adrenaline

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Contents

* [Overview](#overview)
* [Versions](#versions)
* [Issues](#issue)
* [Features](#feature)
* [Future plans](#Future-plans)
* [Installing](#installing)
* [License](#license)

## Overview

Adrenaline sharpens your feelings in mortal danger. Accelerates your running and speed of mining and speed of shooting at times.

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by
[submitting an issue](https://gitlab.com/ZwerOxotnik/adrenaline/issues) to our GitLab Repository or on [mods.factorio.com](https://mods.factorio.com/mod/adrenaline/discussion).

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue](https://gitlab.com/ZwerOxotnik/adrenaline/issues) to our GitLab
Repository or on [mods.factorio.com](https://mods.factorio.com/mod/adrenaline/discussion).

## Future plans

* Add remote interface
* Improve compatibility with scenarios

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to adrenaline_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 1.3.2)

## License

```
The MIT License (MIT)

Copyright (c) 2019-2020 ZwerOxotnik <zweroxotnik@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

[homepage]: http://mods.factorio.com/mod/adrenaline
[Factorio]: https://factorio.com/
